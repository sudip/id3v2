id3v2 (0.1.12+dfsg-2) unstable; urgency=medium

  * QA upload.

  [ Sudip Mukherjee ]
  * Update compat level to 12.
  * Update Standards-Version to 4.5.0.

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Wed, 18 Mar 2020 18:35:55 +0000

id3v2 (0.1.12+dfsg-1) unstable; urgency=medium

  * QA upload.
  * Remove binary files and .git dir to enable importing import via gbp
  * cme fix dpkg-control
  * Moved packaging from SVN to Git
  * debhelper 11

 -- Andreas Tille <tille@debian.org>  Thu, 01 Feb 2018 13:24:36 +0100

id3v2 (0.1.12-3) unstable; urgency=medium

  * QA upload.
  * Ack for NMU. Thanks to Hwei Sheng Teoh <hsteoh@debian.org>.
    (Closes: #559998)
  * Set QA Group as maintainer. (see #770257)
  * Migrations:
      - debian/copyright to 1.0 format.
      - DH level to 10.
  * debian/control:
      - Bumped Standards-Version to 3.9.8.
      - Changed from http to https in Vcs-Browser field and updated the URL.
      - Changed Vcs-Svn field to use canonical URI.
      - Removed obsoleted DM-Upload-Allowed field.
      - Removed the leading article from synopsis.
  * debian/patches/Makefile.patch: added a header.
  * debian/rules:
      - Added DEB_BUILD_MAINT_OPTIONS variable to improve the GCC hardening.
      - Added DEB_LDFLAGS_MAINT_APPEND variable to avoid linking against of
        unnecessary symbols.
  * debian/watch:
      - Improved.
      - Updated to version 4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 16 May 2017 13:08:47 -0300

id3v2 (0.1.12-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Apply Martin Stjernholm's patch to fix charset issues.	closes: #559998

 -- Hwei Sheng Teoh <hsteoh@debian.org>  Tue, 05 Feb 2013 22:07:21 -0800

id3v2 (0.1.12-2) unstable; urgency=low

  * New upload to force rebuild against the latest version of
    libid3 (fixes spelling of "Thrash Metal")			closes: #584983
  * debian/control: Bumped Standards-Version to 3.9.0

 -- Stefan Ott <stefan@ott.net>  Wed, 07 Jul 2010 05:32:23 +0200

id3v2 (0.1.12-1) unstable; urgency=low

  * New upstream version					closes: #580063
  * Added patch to implement -R (rfc822 compliant output)	closes: #198406
  * Dropped initialize-with-NULL.patch (implemented upstream)
  * Fixed debian/watch

 -- Stefan Ott <stefan@ott.net>  Tue, 04 May 2010 05:20:44 +0200

id3v2 (0.1.11-6) unstable; urgency=low

  * Added patch to complain if converting tags fails		closes: #538165
  * Added note about '--' to the man page			closes: #497985
  * Added warning when just one in several files is untagged	closes: #356473
  * debian/control: Bumped Standards-Version to 3.8.4

 -- Stefan Ott <stefan@ott.net>  Fri, 12 Mar 2010 02:49:35 +0100

id3v2 (0.1.11-5) unstable; urgency=low

  * Changed to source "3.0 (quilt)"
  * debian/patches/Makefile.patch:
    - Link against zlib again					closes: #562369
    - Added patch description
  * Added patch to fix some spelling mistakes
  * debian/control:
    - Added DM-Upload-Allowed field to allow maintainer uploads
    - Bumped Standards-Version to 3.8.3
    - Removed explicit quilt build-dependency

 -- Stefan Ott <stefan@ott.net>  Tue, 05 Jan 2010 05:21:30 +0100

id3v2 (0.1.11-4) unstable; urgency=low

  * debian/control:
    - New maintainer						closes: #527480
    - Set debhelper dependency to version 7
    - Build-depend on quilt
    - Bumped Standards-Version to 3.8.1
    - Added ${misc:Depends} dependency
    - Added homepage field
    - Added Vcs-* fields
  * debian/compat: Set compatibility to version 7
  * debian/copyright:
    - Added upstream copyright notice
    - Changed path to license from GPL to GPL-2
  * debian/rules: Rewrote to use current debhelper
  * Added debian/manpages to make sure the man page is processed by debhelper
  * Added usr/share/man/man1 to debian/dirs
  * Added debian/watch
  * Moved changes to the upstream source into debian/patches/*

 -- Stefan Ott <stefan@ott.net>  Sun, 07 Jun 2009 19:14:29 +0200

id3v2 (0.1.11-3) unstable; urgency=low

  * Recompiled for C++ allocator change, closes: #341254

 -- Robert Woodcock <rcw@debian.org>  Sun, 04 Dec 2005 21:12:59 -0800

id3v2 (0.1.11-2) unstable; urgency=low

  * Recompiled for new C++ ABI, closes: #319662

 -- Robert Woodcock <rcw@debian.org>  Tue, 02 Aug 2005 19:27:39 -0700

id3v2 (0.1.11-1) unstable; urgency=low

  * New upstream version, closes: #259919

 -- Robert Woodcock <rcw@debian.org>  Sun, 15 Aug 2004 20:29:08 -0700

id3v2 (0.1.9-2) unstable; urgency=low

  * Rebuild against id3lib 3.8.3-4, remove build-depends on gcc 3.2,
    closes: #236449
  * Edited package description, closes: #209525

 -- Robert Woodcock <rcw@debian.org>  Sun, 18 Apr 2004 07:42:56 -0700

id3v2 (0.1.9-1) unstable; urgency=low

  * New upstream version
  * This one should handle comments correctly, closes: #72927
  * Includes antisegfault patch again, closes: #149343
  * Upstream fixed the mode 600 problem, closes: #158108

 -- Robert Woodcock <rcw@debian.org>  Thu, 13 Mar 2003 21:10:32 -0800

id3v2 (0.1.7-5) unstable; urgency=low

  * Last version didn't autobuild on anything, this should fix that,
    closes: #178175

 -- Robert Woodcock <rcw@debian.org>  Wed, 29 Jan 2003 13:23:37 -0800

id3v2 (0.1.7-4) unstable; urgency=low

  * Rebuilt with g++-3.2 against id3lib 3.8.2

 -- Robert Woodcock <rcw@debian.org>  Tue, 21 Jan 2003 12:23:24 -0800

id3v2 (0.1.7-3) unstable; urgency=low

  * Recompiled - -2 was built against a subtley incompatible id3lib
    snapshot. closes: #157996

 -- Robert Woodcock <rcw@debian.org>  Fri, 23 Aug 2002 20:27:15 -0700

id3v2 (0.1.7-2) unstable; urgency=low

  * Fixed build dependencies

 -- Robert Woodcock <rcw@debian.org>  Sat, 29 Jun 2002 08:19:57 -0700

id3v2 (0.1.7-1) unstable; urgency=low

  * New upstream version, closes: #142243, #142244, #149777

 -- Robert Woodcock <rcw@debian.org>  Thu, 27 Jun 2002 20:23:25 -0700

id3v2 (0.1.3-5) unstable; urgency=low

  * Added patch by Mark Brown to guarantee that newTrackNum can be safely
    freed, preventing a segfault on PPC, closes: #126280

 -- Robert Woodcock <rcw@debian.org>  Sun, 30 Dec 2001 13:20:17 -0800

id3v2 (0.1.3-4) unstable; urgency=low

  * Dunno why -3 got hosed on PPC. Rebuilding it, closes: #126023

 -- Robert Woodcock <rcw@debian.org>  Fri, 21 Dec 2001 01:07:23 +0000

id3v2 (0.1.3-3) unstable; urgency=low

  * Added build depends for libid3-dev, closes: #82973
  * Removed INSTALL file from docs
  * Updated Standards-Version

 -- Robert Woodcock <rcw@debian.org>  Thu, 11 Oct 2001 17:25:50 -0700

id3v2 (0.1.3-2) unstable; urgency=low

  * Recompiled against libid3-3.7-13, closes: #82837

 -- Robert Woodcock <rcw@debian.org>  Thu, 19 Jan 2001 18:07:43 -0700

id3v2 (0.1.3-1) unstable; urgency=low

  * Initial Release.

 -- Robert Woodcock <rcw@debian.org>  Thu, 18 May 2000 21:29:11 -0700

Local variables:
mode: debian-changelog
End:
id3v2 (0.1.7-1) unstable; urgency=low

  * Fixed display/setting genre.

 -- Myers Carpenter <myers@fil.org>  Mon, 15 Apr 2002 21:34:40 -0400

id3v2 (0.1.6-1) unstable; urgency=low

  * Fixed perms on updated file.

 -- Myers Carpenter <myers@fil.org>  Tue, 19 Mar 2002 10:15:03 -0500

id3v2 (0.1.3-1) unstable; urgency=low

  * Initial Release.

 -- Robert Woodcock <rcw@debian.org>  Thu, 18 May 2000 21:29:11 -0700

Local variables:
mode: debian-changelog
End:
